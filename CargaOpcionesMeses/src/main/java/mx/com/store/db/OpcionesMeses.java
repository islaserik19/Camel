package mx.com.store.db;

import lombok.Data;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

import java.io.Serializable;
import java.util.Date;

@Data
@CsvRecord(separator = ",", skipFirstLine = true )
public class OpcionesMeses implements Serializable {

    private int opcionId;
    @DataField(pos = 1)
    private int prefijoTarjeta;
    @DataField(pos = 2)
    private String institutcionBancaria;
    @DataField(pos = 7)
    private String tipoTarjeta;
    @DataField(pos = 3)
    private String nombreBanco;
    @DataField(pos = 4)
    private int estatus;
    @DataField(pos = 8)
    private String tipoPago;
    @DataField(pos = 5)
    private Integer regionId;
    @DataField(pos = 6)
    private String msi;
    private Integer monto3Meses;
    private Integer monto6Meses;
    private Integer monto12Meses;
    private Date creadoEn;
    //-- 0 para garga por api, 1 para carga de archivo
    private Integer cargaArchivo;
    private String exchangeId;
}

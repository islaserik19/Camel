package mx.com.store.db;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ArchivosProcesados implements Serializable {
    private int idArchivo;
    private String nombre;
    private Date createdAt;
    private int registrosProcesados;
    private String exchangeId;
}

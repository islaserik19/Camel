package mx.com.store;

import mx.com.store.db.ArchivosProcesados;
import mx.com.store.db.OpcionesMeses;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * A Camel Java DSL Router
 */
public class MyRouteBuilder extends RouteBuilder {

    private DataFormat bindy;
    private SimpleDateFormat sdf;

    public void configure() {

        this.bindy = new BindyCsvDataFormat(OpcionesMeses.class);
        this.sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        restConfiguration()
                .component("jetty")
                .enableCORS(true)
                .host("{{app.host}}")
                .port("{{app.port}}")
                .corsHeaderProperty("Access-Control-Allow-Origin", "*");

        //--ruta para crear nuevas opciones de pago
        rest()
                .post("/opcionesPago")
                .route()
                .to("direct:createOption");
        //--ruta para cargar archivo
        rest()
                .post("/opcionesPago/cargaArchivo")
                .route()
                .to("direct:uploadDataFile");
        //-- ruta para cconsultar todas las opciones de pago
        rest()
                .get("/opcionesPago/")
                .route()
                .to("direct:getOptions");
        //--ruta para consultar las opciones de pago por Id
        rest()
                .get("/opcionesPago/{id}")
                .route()
                .to("direct:getOptionsById");
        //--ruta para eliminar las opciones de pago
        rest()
                .delete("/opcionesPago/{id}")
                .route()
                .to("direct:deleteOptionsById");
        //-- ruta para consultar los archicos cargados
        rest()
                .get("/opcionesPago/archivos")
                .route()
                .to("direct:uploadedFiles");

        //--ruta para actualizar las opciones de pago
        rest()
                .put("/opcionesPago")
                .consumes("application/json")
                .route()
                .to("direct:updateOptions");

        from("direct:createOption")
                .unmarshal(getJacksonDataFormat(OpcionesMeses.class))
                .process(this::createOption)
                .to("jdbc:dataSource")
                .setBody(constant("{\"create\": \"OK\"}"))
                .end();
//otherwise

        from("direct:uploadDataFile")
                .log("ruta para cargar archivo")
                .pollEnrich("file:/home/Integrator/InputFiles?noop=true&fileName=prefijosMeses.csv")
                .unmarshal(bindy)
                .split(body())
                .process(this::saveSqlData)
                .to("jdbc:dataSource")
                .end();


      /*  from("direct:uploadFile")
                .log("ruta para cargar archivo")
                .pollEnrich("file:/home/Integrator/InputFiles?noop=true&fileName=prefijosMeses.csv")
                .unmarshal(bindy)
                .process(this::processUploadFile)
                .to("jdbc:dataSource")
                .end();*/

        from("direct:getOptions")
                .process(this::getAllOptions)
                .to("jdbc:dataSource")
                .marshal(getJacksonDataFormat(OpcionesMeses.class))
                .end();

        from("direct:getOptionsById")
                .process(this::getOptionsById)
                .to("jdbc:dataSource")
                .marshal(getJacksonDataFormat(OpcionesMeses.class))
                .end();

        from("direct:deleteOptionsById")
                .process(this::deleteOptionsById)
                .to("jdbc:dataSource")
                .end();

        from("direct:uploadedFiles")
                .process(this::getUploadedFiles)
                .to("jdbc:dataSource")
                .marshal(getJacksonDataFormat(ArchivosProcesados.class))
                .end();

        from("direct:updateOptions")
                .unmarshal(getJacksonDataFormat(OpcionesMeses.class))
                .process(this::updateOptions)
                .to("jdbc:dataSource")
                .setBody(constant("{\"update\": \"OK\"}"))
                .end();
    }

    private void createOption(Exchange ex) {
        OpcionesMeses options = ex.getIn().getBody(OpcionesMeses.class);
        String query = "insert into opcionesmeses(prefijoTarjeta, institutcionBancaria, tipoTarjeta, nombreBanco, " +
                "estatus, tipoPago, monto3Meses, monto6Meses, monto12Meses, creadoEn, cargaArchivo, exchangeId) " +
                "values (" + options.getPrefijoTarjeta() + ", '" + options.getInstitutcionBancaria() + "','" + options.getTipoTarjeta() + "','" + options.getNombreBanco() + "'," + options.getEstatus() + ",'" + options.getTipoPago() + "'," +
                options.getMonto3Meses() + "," + options.getMonto6Meses() + "," + options.getMonto12Meses() + ",'" + sdf.format(new Date()) + "'," + options.getCargaArchivo() + ",'" + ex.getExchangeId() + "')";
        ex.getIn().setBody(query);
    }

    private void saveSqlData(Exchange ex) {
        OpcionesMeses options = ex.getIn().getBody(OpcionesMeses.class);
        String[] optMsi = options.getMsi().split("\\,");
        for (String msi : optMsi) {
            String[] arrMsi = msi.split("\\|");
            if (arrMsi.length > 0) {
                switch (arrMsi[0]) {
                    case "3":
                        options.setMonto3Meses(Integer.parseInt(arrMsi[1]));
                        break;
                    case "6":
                        options.setMonto6Meses(Integer.parseInt(arrMsi[1]));
                        break;
                    case "12":
                        options.setMonto12Meses(Integer.parseInt(arrMsi[1]));
                        break;
                }
            }
        }

        String query = "insert into opcionesmeses(prefijoTarjeta, institutcionBancaria, tipoTarjeta, nombreBanco, " +
                "estatus, tipoPago, monto3Meses, monto6Meses, monto12Meses, creadoEn, cargaArchivo, exchangeId) " +
                "values (" + options.getPrefijoTarjeta() + ", '" + options.getInstitutcionBancaria() + "','" + options.getTipoTarjeta() + "','" + options.getNombreBanco() + "'," + options.getEstatus() + ",'" + options.getTipoPago() + "'," +
                options.getMonto3Meses() + "," + options.getMonto6Meses() + "," + options.getMonto12Meses() + ",'" + sdf.format(new Date()) + "'," + options.getCargaArchivo() + ",'" + ex.getExchangeId() + "')";
        ex.getIn().setBody(query);
    }


    private void getAllOptions(Exchange ex) {
        String query = "SELECT * FROM opcionesmeses;";
        ex.getIn().setBody(query);
    }

    private void getOptionsById(Exchange ex) {
        Integer id = ex.getIn().getHeader("id", Integer.class);
        String query = "SELECT * FROM opcionesmeses WHERE opcionesmeses.opcionId =" + id;
        ex.getIn().setBody(query);
    }

    private void deleteOptionsById(Exchange ex) {
        Integer id = ex.getIn().getHeader("id", Integer.class);
        String query = "DELETE FROM opcionesmeses WHERE opcionesmeses.opcionId =" + id;
        ex.getIn().setBody(query);
    }

    private void processUploadFile(Exchange ex) {
        ArrayList<Map<String, String>> dataList = (ArrayList<Map<String, String>>) ex.getIn().getBody();
        String query = "INSERT INTO archivosprocesados(nombre, createdAt, registrosProcesados, exchangeId) " +
                "values('prefijosMeses.csv','" + sdf.format(new Date()) + "'," + dataList.size() + ",'" + ex.getExchangeId() + "')";
        System.out.println(query);
        ex.getIn().setBody(query);
    }

    private void getUploadedFiles(Exchange ex) {
        String query = "SELECT * FROM archivosProcesados";
        ex.getIn().setBody(query);
    }

    private void updateOptions(Exchange ex) {
        //System.out.println(ex.getIn().getBody(String.class));
        OpcionesMeses options = ex.getIn().getBody(OpcionesMeses.class);
        String query = "UPDATE opcionesmeses SET prefijoTarjeta=" + options.getPrefijoTarjeta() + "," +
                "institutcionBancaria='" + options.getInstitutcionBancaria() + "'," + "tipoTarjeta='" +
                options.getTipoTarjeta() + "',nombreBanco='" + options.getNombreBanco() + "',estatus=" +
                options.getEstatus() + ",tipoPago=" + options.getTipoPago() + ",monto3Meses=" + options.getMonto3Meses() +
                ",monto6Meses=" + options.getMonto6Meses() + ",monto12Meses=" + options.getMonto12Meses() +
                ",creadoEn='" + sdf.format(options.getCreadoEn()) + "',cargaArchivo=" + options.getCargaArchivo() + ",exchangeId='"
                + options.getExchangeId() + "' WHERE opcionId=" + options.getOpcionId();

        ex.getIn().setBody(query);
    }

    private JacksonDataFormat getJacksonDataFormat(Class<?> unmarshalType) {
        JacksonDataFormat format = new JacksonDataFormat();
        format.setUnmarshalType(unmarshalType);
        return format;
    }


}

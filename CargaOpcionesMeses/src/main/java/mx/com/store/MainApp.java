package mx.com.store;

import org.apache.camel.CamelContext;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.support.SimpleRegistry;
import org.apache.commons.dbcp.BasicDataSource;

import javax.sql.DataSource;
import java.util.Optional;

/**
 * A Camel Application
 */
public class MainApp {

    private static CamelContext context;

    public static void main(String... args) throws Exception {

        SimpleRegistry reg = new SimpleRegistry();
        context = new DefaultCamelContext(reg);

        PropertiesComponent pc = new PropertiesComponent();
        pc.setLocation("classpath:config.properties");
        context.setPropertiesComponent(pc);

        DataSource dataSource = setupDataSource();
        reg.bind("dataSource", dataSource);

        MyRouteBuilder myRouteBuilder = new MyRouteBuilder();
        context.addRoutes(myRouteBuilder);
        context.start();

    }

    private static DataSource setupDataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUsername(getProp("app.sql.user"));
        ds.setPassword(getProp("app.sql.password"));
        ds.setUrl(getProp("app.sql.url"));
        return ds;
    }

    private static String getProp(String key){
        String value = "";
        Optional<String> prop = context.getPropertiesComponent().resolveProperty(key);
        if (prop.isPresent()) {
            value = prop.get();
        }
        return value;
    }

}

